## About Aplikasi Manajemen Tim / Kelompok Kerja

Sebuah perusahaan ingin membuat sistem yang dimaksudkan untuk mengelola tim atau kelompok kerja. Pengelolaan tim tersebut dilakukan agar perusahaan mengetahui detail struktur dari masing-masing tim mulai dari penanggung jawab, ketua dan anggota.

## Task
Tugas teman2 adalah membuat aplikasi manajemen tim / kelompok kerja dengan ketentuan aplikasi sebagai berikut
- [ ]	Dalam sebuah tim terdapat satu penanggung jawab
- [ ]	Dalam sebuah tim terdapat satu ketua
- [ ]	Dalam sebuah tim terdapat minimal 3 anggota
- [ ]	Satu pegawai hanya dapat mengisi satu role dalam sebuah tim.
- [ ]	Satu pegawai hanya dapat terdaftar pada maksimal 2 tim saja.
- [ ]	Semua pegawai dapat mendaftarkan timnya masing-masing.
- [ ]	Pegawai yang mendaftarkan timnya secara otomatis akan menjadi penanggung jawab dari tim tersebut.
- [ ]	Setiap Pegawai dapat melihat semua tim yang telah terdaftar.
- [ ]	Hanya penanggung jawab yang dapat merubah dan menghapus timnya.

Ketentuan teknis pembuatan aplikasi
- [X]	Project dibuat menggunakan framework Laravel
- [ ]	Perhatikan validasi data.
- [ ]	User Interface sesuai preferensi masing2.
- [ ]	Silahkan gunakan laravel authentication untuk membuat sistem autentikasi.
- [ ]	Diperbolehkan menggunakan plugin
- [ ]	Silahkan gunakan seeder/faker atau plugin lainya untuk membuat data pegawai (tidak perlu ada manajemen user/pegawai)
- [ ]	Gunakan Laravel Migration untuk majemen database.
- [ ]	Gunakan Laravel Eloquent untuk berinteraksi dengan database.
- [ ]	Maksimalkan konsep MVC yang disediakan oleh Laravel secara baik dan benar.
- [ ]	Untuk Projeknya silahkan di-push ke Gitlab.
